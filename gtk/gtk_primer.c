/*
 * G T K _ P R I M E R . C
 *
 * Tests basic GTK functionality.
 *
 * Author: Herrmann, Max
 * Date: Fri SEP 08 2023
 * Revision:  0   Fri SEP 08 2023   First version. / Herrmann, Max
 *
 * Compile with:
 * gcc ~/git/c-exercises/gtk/gtk_primer.c -I/usr/include/gtk-4.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/include/cairo -I/usr/include/pango-1.0/pango -I/usr/include/pango-1.0 -I/usr/include/harfbuzz -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/graphene-1.0 -I/usr/lib64/graphene-1.0/include -L/usr/lib64 -lgtk-4 -lgobject-2.0 -lgio-2.0 -lglib-2.0
 */

#include <gtk-4.0/gtk/gtk.h>

static void on_key_press(
                         GtkEventControllerKey *self,
                         guint keyval,
                         guint keycode,
                         GdkModifierType state,
                         gpointer user_data
                         ) {

  g_print("key has been pressed (0x%x).\n", keycode);
  
}

static void on_activate(GtkApplication *app) {

  // create a window
  GtkWidget *window = gtk_application_window_new(app);

  // create an event controller for key strokes
  GtkEventController *event_controller_key = gtk_event_controller_key_new();

  // add controller to window s.t. it will receive events
  gtk_widget_add_controller(window, event_controller_key);

  // connect a key event with a callback function
  g_signal_connect(event_controller_key, "key-pressed", G_CALLBACK (on_key_press), NULL);

  // show text
  GtkWidget *text_view;
  GtkTextBuffer *buffer;

  text_view = gtk_text_view_new();
  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (text_view));
  gtk_text_buffer_set_text(buffer, "hey", -1);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (text_view), FALSE);
  gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (text_view), FALSE);

  gtk_window_set_child (GTK_WINDOW (window), text_view);  

  gtk_window_present (GTK_WINDOW (window));
  
}

int main(int argc, char *argv[]) {

  g_print("initializing...\n");

  GtkApplication *app = gtk_application_new("com.example.GtkApplication", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);
  
  return g_application_run (G_APPLICATION (app), argc, argv);
  
}

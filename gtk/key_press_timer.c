/*
 * K E Y _ P R E S S _ T I M E R . C
 *
 * Counts clock cycles a key has been pressed.
 *
 * Author: Herrmann, Max
 * Date: Fri SEP 15 2023
 * Revision:  0   Fri SEP 15 2023   First version. / Herrmann, Max
 *
 * Compile with:
 * gcc ~/git/c-exercises/gtk/key_press_timer.c -o key_press_timer -I/usr/include/gtk-4.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/include/cairo -I/usr/include/pango-1.0/pango -I/usr/include/pango-1.0 -I/usr/include/harfbuzz -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/graphene-1.0 -I/usr/lib64/graphene-1.0/include -L/usr/lib64 -lgtk-4 -lgobject-2.0 -lgio-2.0 -lglib-2.0
 */

#include <gtk-4.0/gtk/gtk.h>
#include <time.h>

int key_is_released = 1;
int clock_runs = 0;
clock_t start;

static void on_key_release(
                         GtkEventControllerKey *self,
                         guint keyval,
                         guint keycode,
                         GdkModifierType state,
                         gpointer user_data
                         ) {

  key_is_released = 1;

  if (clock_runs) {
    clock_t diff = clock() - start;
    g_print("key was pressed for %d clock cycles.\n", diff);
    clock_runs = 0;
  }
    
  
}
  
static void on_key_press(
                         GtkEventControllerKey *self,
                         guint keyval,
                         guint keycode,
                         GdkModifierType state,
                         gpointer user_data
                         ) {

  if (key_is_released) {
    g_print("key press detected.\n");
    start = clock();
    clock_runs = 1;
    key_is_released = 0;
  }
  
  
}

static void on_activate(GtkApplication *app) {

  // create a window
  GtkWidget *window = gtk_application_window_new(app);

  // create an event controller for key strokes
  GtkEventController *event_controller_key = gtk_event_controller_key_new();

  // add controller to window s.t. it will receive events
  gtk_widget_add_controller(window, event_controller_key);

  // connect key events with callback functions
  g_signal_connect(event_controller_key, "key-pressed", G_CALLBACK (on_key_press), NULL);
  g_signal_connect(event_controller_key, "key-released", G_CALLBACK (on_key_release), NULL);

  // show text
  GtkWidget *text_view;
  GtkTextBuffer *buffer;

  text_view = gtk_text_view_new();
  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (text_view));
  gtk_text_buffer_set_text(buffer, "hey", -1);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (text_view), FALSE);
  gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (text_view), FALSE);

  gtk_window_set_child (GTK_WINDOW (window), text_view);  

  gtk_window_present (GTK_WINDOW (window));
  
}

int main(int argc, char *argv[]) {

  g_print("initializing...\n");

  GtkApplication *app = gtk_application_new("com.example.GtkApplication", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);
  
  return g_application_run (G_APPLICATION (app), argc, argv);
  
}

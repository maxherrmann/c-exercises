#include <stdio.h>
#include "..\readLine.h"

int main() {
	
	/* Allocate memory for an array of size 46 with `char' elements */
	char line[46];
	
	/* Call the test candidate `readLine()' */
	readLine(line);
	
	/* Write `readLine()'´s output to standard out */
	printf("%s", line);
	
	return 0;
}
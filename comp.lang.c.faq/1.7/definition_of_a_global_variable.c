/*
 *
 * Compile with:
 * gcc -o declaration_and_definition_of_a_global_variable.exe definition_of_a_global_variable.c use_of_a_global_variable_1.c use_of_a_global_variable_2.c
 */

#include "declaration_of_a_global_variable.h"

/* declare functions */
void tell_me_the_value_of_the_variable_1();
void tell_me_the_value_of_the_variable_2();

int a_global_variable = 5;

void main() {
	tell_me_the_value_of_the_variable_1();
	tell_me_the_value_of_the_variable_2();
}
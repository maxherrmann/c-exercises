/*
 *   M Y L I B . C
 *
 *   Testing the creation of libraries with gcc
 *
 *   Compile with:
 *   gcc -shared -fPIC -o libmylib.so mylib.c
 */

#include <math.h>

double mysqrt(double arg) {
  return sqrt(arg);
}

/*
 *  M Y L I B _ C L I E N T . C
 *
 *  Testing the use of libraries
 *
 *  Compile with:
 *  gcc -o mylib_client mylib_client.c -L. -lmylib
 */

#include <stdio.h>

double mysqrt(double);

int main(int narg, char **arg) {

  printf("Square root of 4 is %.0f.\n", mysqrt(4));
  
}

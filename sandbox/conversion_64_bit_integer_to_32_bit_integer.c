#include <time.h>
#include <stdio.h>

/*
 * This code shows that type `time_t' has 8 byte (64 bit).
 * The function `takes_unsigned_integer' takes a 32-bit integer as argument.
 * Compiling shows that this function also accepts a 64-bit integer which will then be truncated.
 * The most signficant bits will be cut off.
 */

void show_bit_sequence(long unsigned int arg, int number_of_bits) {
	long unsigned int mask = 1;
	for (int i = number_of_bits; i >= 0; --i) {
		if ( (mask << i) & arg )
			printf("1");
		else
			printf("0");
	}
	printf("\n");
}

void takes_unsigned_integer(unsigned int arg) {
	printf("32-bit = %032i", 0);
	show_bit_sequence(arg, 32);
}

int main() {
	time_t current_time = time(NULL);
	printf("`time_t' has %i byte.\n", sizeof(time_t));
	printf("64-bit = ");
	show_bit_sequence(current_time, 64);
	takes_unsigned_integer(current_time);
	return 0;
}
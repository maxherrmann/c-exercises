#include <stdlib.h>
#include <stdio.h>

int main(int nargs, char** args) {
	
	printf("Return value is %f", atof("no value"));
	
	if (atof("no value") == 0.0)
		printf("The return value is a real 0.0\n");
	else
		printf("The return value is not a real 0.0\n");
	
	return 0;
	
}
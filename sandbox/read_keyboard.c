/*
 * R E A D _ K E Y B O A R D . H
 *
 * Prints which key was pressed for how long.
 *
 * Based on this stack overflow entry:
 *
 * https://stackoverflow.com/a/20946151/5052877
 *
 * Author: Herrmann, Max
 * Date: Fri AUG 25 2023
 * Revision:  0   Fri AUG 25 2023   First version / Herrmann, Max
 *            1   Sun AUG 27 2023   Added comments / Herrmann, Max
 *                                  Added infinite loop with call to read() / Herrmann, Max
 */

#include <linux/input.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main () {

  /*
   * (character special) file describing the keyboard device
   * path taken from stack overflow post as given above
   *
   * Wikipedia says:
   * (https://en.wikipedia.org/wiki/Device_file#Character_devices)
   *
   * "`Character special files' or `character devices' provide unbuffered, direct
   *  access to the hardware device."
   *
   * `platform-i8042-serio-0-event-kbd' is a symbolic link pointing to `event4'.
   * where `event4' is a `character special file'
   * it cannot be displayed in an editor.
   *
   * $> pwd
   * $> /dev/input
   * $> ls -l
   * ...
   * crw-rw---- ... event4
   */
  const char* device = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
  // const char* device = "/dev/input/by-path/pci-0000:00:15.1-platform-i2c_designware.1-event";
  // const char* device = "/dev/input/by-path/platform-INT33D5:00-event";
  // const char* device = "/dev/input/platform-PNP0C14:01-event";

  /*
   * https://www.man7.org/linux/man-pages/man2/open.2.html
   *
   * "open and possibly create a file"
   *
   * header: fcntl.h
   *
   * Options chosen are `read only'.
   */
  int device_file_descriptor = open(device, O_RDONLY);

  printf("The device file descriptor is %d.\n", device_file_descriptor);

  /*
   * allocate memory for `input_event' structure
   *
   * https://www.kernel.org/doc/html/v4.15/input/input.html#event-interface
   *
   * struct input_event {
   *
   *    struct time_val time;
   *    unsigned short type;
   *    unsigned short code;
   *    unsigned int value;
   *
   * }
   */
  struct input_event event;

  /* return value for error handling */
  int return_value;

  /* number of repeated key presses */
  int number_of_repeated_key_presses = 0;

  /* read pressed key */
  for(;;) {

    /*
     * https://man7.org/linux/man-pages/man2/read.2.html
     *
     * "read() attempts to read up to `count' bytes from file descriptor
     * `device_file_descriptor' into the buffer starting at `&event'."
     *
     * header: unistd.h
     */
    return_value = read(device_file_descriptor, &event, sizeof(event));

    /* error case: exit */
    if (return_value < 0) {

      printf("Error reading key.\n");
      return -1;
     
    }

    /*
     * inspect event structure
     *
     * https://www.kernel.org/doc/html/v4.15/input/event-codes.html#event-types
     *
     * "EV_KEY: Used to describe state changes of keyboards, buttons, or other key-like devices"
     */
    if (event.type == EV_KEY) {

      if (event.value == 1)
        printf("A key (%x) has been pressed.\n", event.code);

      if (event.value == 0) {

        /* allocate memory for backspace string */
        char backspaces [number_of_repeated_key_presses + 1];
        backspaces[0] = 0;
        for(int i = 0; i < number_of_repeated_key_presses; ++i) {
          backspaces[i] = '\b';
          backspaces[i+1] = 0;
        }
        
        printf("\b%sA key (%x) has been released.\n", &backspaces[0], event.code);

        /* reset counter */
        number_of_repeated_key_presses = 0;
      }

      /* count repetitions */
      if (event.value == 2)
        ++number_of_repeated_key_presses;
      
    }

  }
  
}

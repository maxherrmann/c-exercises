#include <stdio.h>

/*
 * `The C Programming Language' says that
 *
 * switch (expression) {
 *     case const-expr: statements
 *     case const-expr: statements
 *     default: statements 
 * }
 *
 * is the general form of a switch statement.
 *
 * Then why does the code below print the string "Either 0 or 1." to standard out?
 * I would have expected it to do nothing, since there is nothing to do for case `0', and case `1' does not apply
*/

int main() {
	char character = '0';
	switch (character) {
		case '0': printf("Found `0'.\n");
		// `case '0'' only serves as an entry point for the switch case; everything past it will be executed; this is considered `fall-trough' behavior of a switch statement
		case '1': printf("Either 0 or 1.\n");
	}
}
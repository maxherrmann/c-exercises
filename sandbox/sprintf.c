#include <stdio.h>
#include <stdlib.h>
#include <time.h>

main () {

	double normalized_random_number;
	char normalized_random_number_string[16];

	srand(time(NULL));
	normalized_random_number = rand() * 1.0 / RAND_MAX;
int sprintf_return_value = sprintf(&(normalized_random_number_string[0]), "%.2f\n", normalized_random_number);
	
	printf("sprintf return value: %d\n", sprintf_return_value);
	printf("randomized number: %.2f\n", normalized_random_number);
	printf("randomized number string: %s\n", normalized_random_number_string);
	
}


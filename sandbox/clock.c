#include <math.h>
#include <stdio.h>
#include <time.h>

int main() {
	double val = 0.0;
	clock_t start, end;
	start = clock();
	for (int i = 0; i < 10000000; ++i)
		val += acos(1.);
	end = clock();
	printf("sizeof(clock_t) = %i byte.\n", sizeof(clock_t));
	printf("start: %u\n", start);
	printf("end: %u\n", end);
	printf("time elapsed: %e\n", ((double) end - start)/CLOCKS_PER_SEC);
}
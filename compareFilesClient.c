#include <stdio.h>

int compareFiles(char*, char*, int);

int main(int numberOfArguments, char** arguments) {
	
	/* 
	 * Check if there are at least three user-supplied parameters 
	 * 1st parameter: name of 1st file
	 * 2nd parameter: name of 2nd file
	 * 3rd parameter: maximum number of characters under comparison
	 */
	if (numberOfArguments < 4) return -1;
	
	/* Assign arguments to file names */
	char* file1 = arguments[1];
	char* file2 = arguments[2];
	
	/* Extract integer from 3rd user-supplied argument string */
	int i, digit;
	i = 0;
	int maximumNumberOfCharacters = 0;
	while ((digit = arguments[3][i++]))
		maximumNumberOfCharacters = maximumNumberOfCharacters * 10 + (digit - 0x30);
	
	/* Call compare function and print evaluation */
	if (!compareFiles(file1, file2, maximumNumberOfCharacters))
		printf("Files match up to first %i characters.\n", maximumNumberOfCharacters);
	else
		printf("Files don't match.\n");
	return 0;
}
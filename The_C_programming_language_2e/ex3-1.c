/*
 * Example 1:
 * 
 * x = 4
 * v = (0, 1, 3, 4, 7, 8, 12, 16, 20, 22)
 * n = 10
 * 
 * mid = 5 which maps to 8
 * 
 * Since x = 4 < 8, hi = 5
 * 
 * new middle element is 0 + (5 - 2) / 2 = 1 which maps to 1
 *
 * Since x = 4 > 1, lo = 1
 *
 * new middle element is 1 + (4 - 1) / 2 = 2 which maps to 3
 *
 * Since x = 4 > 3, lo = 2
 * 
 * new middle element is 2 + (4 - 2) / 2 = 3 which maps to 4
 * 
 * Since x == 4, the loop terminates
 */
 
/*
 * Example 2:
 *
 * x = 0
 * v = (0, 1, 2)
 * n = 3
 * 
 * mid = (2 - 0)/2 = 1 which maps to 1
 * 
 * Since x = 0 < 1, hi = 1
 *
 * new middle element is 0 + (1 - 0)/2 = 0
 * 
 * Since x == v[0], loop terminates
 * 
 */
 
/*
 * Example 3:
 *
 * x = 2
 * v = (0, 1, 2)
 * n = 3
 * 
 * mid = 3/2 = 1 which maps to 1
 *
 * Since x = 2 > 1, lo = 1
 *
 * new middle element is 1 + (2 - 1)/2 = 1 which maps to 1
 * 
 * Since x = 2 > 1, lo = 1
 *
 * Loop does not terminate.
 * 
 */
 
#include <stdio.h>

// searches for x in v with n elements
// returns index if found; -1 otherwise
// expects v to be sorted with increasing values
int binarysearch(int x, int v[], int n) {
	
	// limits of interval
	int lo, hi, mid;
	
	// initialize limits of interval
	lo = 0;
	hi = n-1;
	mid = n/2;
	
	// repeat as long as search interval is not empty
	while (lo != hi) {
		
		// is x greather than middle element?
		if (x < v[mid])
			hi = mid - 1;
		else if (x > v[mid])
			lo = mid + 1;
		
		mid = (lo + hi)/2;
		
		// found x in v
		if (x == v[mid])
			return mid;
				
	}

	// search interval is empty: x does not match any element in v
	return -1;
}

int compare_int(const void* arg1, const void* arg2) {
	int int1, int2;
	int1 = *((int*) arg1);
	int2 = *((int*) arg2);
	if (int1 > int2) return 1;
	if (int1 < int2) return -1;
	return 0;
}

int binary_search_with_exactly_one_test_in_loop(int x, int v[], int n) {
		// limits of interval
	int lo, hi, mid;
	
	// initialize limits of interval
	lo = 0;
	hi = n-1;
	
	// repeat as long as search interval is not empty
	while (lo <= hi) {
		
		mid = (lo + hi)/2;
		
		// is x greather than middle element?
		if (x < v[mid])
			hi = mid - 1;
		else
			lo = mid + 1;
				
	}
	
	// found x in v
	if (x == v[mid])
		return mid;

	// search interval is empty: x does not match any element in v
	return -1;
}
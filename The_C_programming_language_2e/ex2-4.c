// Exercise 2-4. Write an alternate version of `squeeze(s1,s2)' that deletes each character in `s1' that matches any character in the _string_ `s2'.
#include <stdio.h>

// brute force
void squeeze(char* s1, const char* s2) {
	
	int i1, i2, j;
	i1 = i2 = j = 0;
	char c1, c2;
	
	// outer loop: characters of string 2
	for (; (c2 = s2[i2]) != '\0'; ++i2) {
		
		// inner loop: characters of string 1
		for (; (c1 = s1[i1]) != '\0'; ++i1) {
			if (s1[i1] != c2)
				s1[j++] = c1;			
		}

		// terminate string
		s1[j] = '\0';
		
		// reset counters
		i1 = j = 0;
	}

} 
// Exercise 2-9. In a two's complement number system, `x &= (x-1)' deletes the rightmost l-bit in `x'. Explain why. Use this observation to write a faster versionof bitcount.

/*
 * Explanation:
 * ------------
 * 
 * a - b = a - b + (2^n - 2^n) = a + (2^n - b) - 2^n = a + (2^n - b + (1 - 1)) - 2^n = a + ((2^n - 1) - b + 1) - 2^n = a + (1's complement of b + 1)) - ignoring overflow
 * 
 * x - 1 = x + ((1's complement of 1) + 1) = x + (1...1)
 * 
 * x &= (x-1) <=> x = x & (x + 1...1)
 * 
 * Since the bits to the right of the right-most 1-bit are all 0, the adding of 1...1 to x will cause the bits right to the rightermost 1-bit to change to `1', and the rightermost 1-bit change to 0, hence being "deleted".
 *
 */

#include "setbits.h"

int bitcount(unsigned int x) {
	
	if (!x) return 0;

	int count = 0;
	while (!(x &= (x-1))) ++count;
	
	return ++count;
	
}
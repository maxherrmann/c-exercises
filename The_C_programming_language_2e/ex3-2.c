#include <stdio.h>
#include <stdlib.h>

void escape(char **s, const char *t) {
	
	// determine length of source string
	int i = 0, length_of_string_t = 0, character_count = 0;
	while(t[i++])
		length_of_string_t++;
	
	// allocate memory for destination string
	*s = calloc(4*length_of_string_t, sizeof(char));
	
	// deal with non-printing characters	
	i = 0;
	while (t[i]) {
		switch (t[i]) {
			/* SOH, STX, ETX, EOT, ENQ, ACK */
			case '\1': case '\2': case '\3': case '\4': case '\5': case '\6':
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = t[i] + 0x30;
				break;				
			case '\a': /* alert (bell) - BEL (0x7) */ 
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = 'a';
				break;
			case '\b': /* backspace - BS (0x8) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = 'b';
				break;	
			case '\f': /* formfeed - FF (0xC) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = 'f';
				break;
			case '\n': /* newline - LF (0xA) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = 'n';
				break;
			case '\r': /* carriage return - CR (0xD) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = 'r';
				break;	
			case '\t': /* horizontal tab - HT (0x9) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = 't';
				break;
			case '\v': /* vertical tab - VT (0xB) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = 'v';
				break;
			case '\xE': /* shift out - SO (0xE) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '1';
				(*s)[character_count++] = '6';
				break;
			case '\xF': /* shift in - SI (0xF) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '1';
				(*s)[character_count++] = '7';
				break;
			case '\x10': /* data link escape - DLE/DC0 (0x10) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '2';
				(*s)[character_count++] = '0';
				break;
			case '\x11': /* device control 1 - DC1 (0x11) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '2';
				(*s)[character_count++] = '1';
				break;
			case '\x12': /* device control 2 - DC2 (0x12) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '2';
				(*s)[character_count++] = '2';
				break;
			case '\x13': /* device control 3 - DC3 (0x13) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '2';
				(*s)[character_count++] = '3';
				break;
			case '\x14': /* device control 4 - DC4 (0x14) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '2';
				(*s)[character_count++] = '4';
				break;
			case '\x15': /* negative acknowledge - NAK/ERR (0x15) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '2';
				(*s)[character_count++] = '5';
				break;
			case '\x16': /* synchronous idle - SYN/SYNC (0x16) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '2';
				(*s)[character_count++] = '6';
				break;
			case '\x17': /* end of transmission block - ETB/LEM (0x17) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '2';
				(*s)[character_count++] = '7';
				break;
			case '\x18': /* cancel - CAN/S0 (0x18) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '3';
				(*s)[character_count++] = '0';
				break;
			case '\x19': /* end of medium - EM/S1 (0x19) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '3';
				(*s)[character_count++] = '1';
				break;
			case '\x1A': /* substitute - SUB/S2 (0x1A) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '3';
				(*s)[character_count++] = '2';
				break;
			case '\x1B': /* escape - ESC/S3 (0x1B) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '3';
				(*s)[character_count++] = '3';
				break;
			case '\x1C': /* file separator - FS/S4 (0x1C) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '3';
				(*s)[character_count++] = '4';
				break;
			case '\x1D': /* group separator - GS/S5 (0x1D) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '3';
				(*s)[character_count++] = '5';
				break;
			case '\x1E': /* record separator - RS/S6 (0x1E) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '3';
				(*s)[character_count++] = '6';
				break;
			case '\x1F': /* unit separator - US/S7 (0x1F) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '0';
				(*s)[character_count++] = '3';
				(*s)[character_count++] = '7';
				break;	
			case '\x5C': /* backslash - '\\' (0x5C) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '\\';
				break;				
			case '\x7F': /* DEL (0x7F) */
				(*s)[character_count++] = '\\';
				(*s)[character_count++] = '1';
				(*s)[character_count++] = '7';
				(*s)[character_count++] = '7';
				break;				
			// printable characters
			default: {
				(*s)[character_count++] = t[i];
				break; /* TCPL: "As a matter of good form, put a break after the last case" */
			}
		}
		++i;
	}
	/* terminating 0 character */
	(*s)[character_count] = '\0';
}

int is_escape_symbol(char arg) {
	// anything between `a' and `v'
	if ((arg > 0x60) && (arg < 0x77))
		return 1;
	// backslash
	if (arg == '\\')
		return 1;
	return 0;
}

void deescape(char** s, const char* t) {

	// determine length of source string
	int i = 0, length_of_string_t = 0, character_count = 0;
	while(t[i++])
		length_of_string_t++;
	
	// allocate memory for destination string
	*s = calloc(length_of_string_t, sizeof(char));
	
	// deal with non-printing characters
	char escape_number;
	i = 0;
	while (t[i]) {
		
		// detect escape character
		if (t[i] == '\\') {
			
			// test if first escape character is a letter
			if (is_escape_symbol(t[++i])) {
				
				// map letter to its respective ASCII code
				switch (t[i]) {
					case 'a': /* alert (bell) - BEL (0x7) */ 
						(*s)[character_count++] = '\a';
						break;
					case 'b': /* backspace - BS (0x8) */
						(*s)[character_count++] = '\b';
						break;
					case 'f': /* formfeed - FF (0xC) */
						(*s)[character_count++] = '\f';
						break;
					case 'n': /* newline - LF (0xA) */
						(*s)[character_count++] = '\n';
						break;
					case 'r': /* carriage return - CR (0xD) */
						(*s)[character_count++] = '\r';
						break;
					case 't': /* horizontal tab - HT (0x9) */
						(*s)[character_count++] = '\t';
						break;
					case 'v': /* vertical tab - VT (0xB) */
						(*s)[character_count++] = '\v';
						break;
					case '\\': /* backslash - '\\' (0x5C) */
						(*s)[character_count++] = '\\';
						break;					
				}
				// leave escape sequence
				++i;
			}
			else {
						
				// compute escape number
				char base = 64;
				escape_number = 0;
				for (int j = 0; j < 3; ++j) {
					escape_number += (t[i++] - 0x30) * base;
					base /= 8;
				}
				
				(*s)[character_count++] = escape_number;
			}
		} 
		else {
			(*s)[character_count++] = t[i++];
			continue;
		}
		
	}
	/* terminating 0 character */
	(*s)[character_count] = '\0';

}
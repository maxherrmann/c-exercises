#include <math.h>
#include <stdlib.h>

/* 
 * Converts integer n to base-b integer, and writes it to string s.
 *
 * Examples:
 * ---------
 * n = 10, b = 10   => s = "10"
 * n = 10, b = 2    => s = "1010"
 * n = 80, b = 16   => s = "50"
 * n = -1024, b = 8 => s = "-2000"
 *
 * The function cannot handle INT_MIN.
 */
void itob(int n, char** s, unsigned char b) {
	
	// allocate memory for digits string
	*s = calloc(256, sizeof(char));
	
	// special case: n = 0
	if (!n) {
		(*s)[0] = '0';
		(*s)[1] = '\0';
		return;
	}
	
	// prepend minus sign, if required
	int j;
	if (n < 0) {
		n = -n;
		(*s)[0] = '-';
		j = 1;
	} else j = 0;	
	
	// compute largest power of base
	int power = (int) (log(n) / log(b));
	
	// determine digit for every power
	int remainder = n;
	int digit;
	for (int i = power; i > -1; --i, ++j) {
		digit = remainder / ((int) pow(b, i));
		remainder -= digit * pow(b, i);
		if (digit < 10)
			(*s)[j] = digit + 0x30;
		else
			(*s)[j] = digit + 55;
	}
	
	// terminating null character
	(*s)[j] = '\0';

}
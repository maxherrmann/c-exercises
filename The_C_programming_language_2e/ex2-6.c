// Exercise 2-6. Write a function `setbits(x,p,n,y)' that returns `x' with the `n' bits that begin at position `p' set to the rightmost `n' bits of `y', leaving the other bits unchanged.

int setbits(int x, unsigned int p, unsigned int n, int y) {
	
	if (!n) return x;
	
	int mask = (((~((unsigned int) 0)) >> (32 - n)) << p);
	return (y & mask) | (x & (~mask));
	
}
// Exercise 2-8. Write a function `rightrot(x,n)' that returns the value of the integer `x' rotated to the right by `n' bit positions.

#include "setbits.h"

int rightrot(int x, unsigned n) {
	
	if (!n) return x;
	
	int right_rotated_by_n = setbits(x, 0, 32-n, x >> n);
	right_rotated_by_n = setbits(right_rotated_by_n, 31-n, n, x << 32-n);	
	
	return right_rotated_by_n;
	
}
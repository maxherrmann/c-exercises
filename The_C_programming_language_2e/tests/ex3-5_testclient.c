#include "..\ex3-5.h"

#include <stdio.h>
#include <stdlib.h>

int main(int nargs, char** args) {
	
	if (nargs != 2) {
		printf("Missing argument: base\n");
		return -1;
	}
	
	int base = atoi(args[1]);
	
	int character, i;
	char string[64];
	char* number_string;
	
	while((character = getchar()) != EOF) {
		string[i++] = character;
		if (character == '\n') {
			string[i] = '\0';
			itob(atoi(string), &number_string, base);
			printf("%s\n", number_string);
			i = 0;
		}
	}
}
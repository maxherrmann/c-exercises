// Exercise 2-4. Write an alternate version of `squeeze(s1,s2)' that deletes each character in `s1' that matches any character in the _string_ `s2'.
#include <string.h>
#include <stdio.h>

#include "tinytest.h"
#include "..\squeeze.h"

TINYTEST_DECLARE_SUITE(SQUEEZE);

int squeeze_should_accept_two_strings(const char* pName) {
	char* string1 = "";
	char* string2 = "";
	squeeze(string1, string2);
	// invariantly true if it was possible to call `squeeze' with two string parameters
	TINYTEST_ASSERT(1);
	return 1;
}

int squeeze_should_not_do_anything_if_the_second_string_is_empty() {
	char* string1 = "ab";
	char* string2 = "";
	squeeze(string1, string2);
	TINYTEST_ASSERT(!strcmp(string1, "ab"));
	return 1;
}

int squeeze_should_delete_every_occurance_of_the_character_c() {
	char string1[] = "ac";
	char string2[] = "c";
	squeeze(&(string1[0]), &(string2[0]));
	TINYTEST_ASSERT(!strcmp(string1, "a"));
	return 1;
}

int squeeze_should_delete_every_occurance_of_the_character_0_and_pound() {
	char string1[] = "##Jkd0#vbs400qpoe#";
	char* string2 = "0#";
	squeeze(&(string1[0]), string2);
	TINYTEST_ASSERT(!strcmp(string1, "Jkdvbs4qpoe"));
	return 1;
}

// Define test suite
TINYTEST_START_SUITE(SQUEEZE);
	TINYTEST_ADD_TEST(squeeze_should_accept_two_strings,NULL,NULL);
	TINYTEST_ADD_TEST(squeeze_should_not_do_anything_if_the_second_string_is_empty,NULL,NULL);
	TINYTEST_ADD_TEST(squeeze_should_delete_every_occurance_of_the_character_c,NULL,NULL);
	TINYTEST_ADD_TEST(squeeze_should_delete_every_occurance_of_the_character_0_and_pound,NULL,NULL);
TINYTEST_END_SUITE();

// Run test suite
TINYTEST_START_MAIN();
	TINYTEST_RUN_SUITE(SQUEEZE);
TINYTEST_END_MAIN();
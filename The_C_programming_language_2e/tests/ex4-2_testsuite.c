/*
 *
 * Exercise 4-2. Extend atof to handle scientific notation of the form
 *
 * 123.45e-6
 * 
 * where a floating-point number may be followed by e or E and an optionally
 * signed exponent.
 *
 *
 * Compile with:
 * gcc -o ex4-2_testsuite.exe ex4-2_testsuite.c ..\ex4-2.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#include "..\ex4-2.h"
#include "tinytest.h"

int atof_scientific_can_handle_1_dot_0e0() {
	if (atof_scientific("1e0") == 1.0)
		return 1;
	return 0;
}

int atof_scientific_can_handle_2_dot_0e_minus_1() {
	if (atof_scientific("2e-1") == 0.2)
		return 1;
	return 0;
}

int atof_scientific_can_handle_weird_doubles() {
	char* val_string = calloc(13, sizeof(char));
	sprintf(val_string, "%.5e", atof_scientific("-7.67944e-28"));
	if (strcmp(val_string, "-7.67944e-028") == 0)
		return 1;
	printf("Expected: -7.67944e-28, Actual: %s\n", val_string);
	return 0;
}

TINYTEST_START_SUITE(ATOF);
	TINYTEST_ADD_TEST(atof_scientific_can_handle_1_dot_0e0, NULL, NULL);
	TINYTEST_ADD_TEST(atof_scientific_can_handle_2_dot_0e_minus_1, NULL, NULL);
	TINYTEST_ADD_TEST(atof_scientific_can_handle_weird_doubles, NULL, NULL);
TINYTEST_END_SUITE();

TINYTEST_MAIN_SINGLE_SUITE(ATOF);
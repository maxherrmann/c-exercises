// Exercise 2-6. Write a function `setbits(x,p,n,y)' that returns `x' with the `n' bits that begin at position `p' set to the rightmost `n' bits of `y', leaving the other bits unchanged.

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "tinytest.h"
#include "..\setbits.h"

// declare test suite
TINYTEST_DECLARE_SUITE(SETBITS);

void bits_of_32bit_signed_int_to_string(int arg, char* string) {
	for(int i = 0; i < 32; ++i) {
		if ((1 << i) & arg)
			string[31 - i] = '1';
		else
			string[31 - i] = '0';
	}
}

// tests
int setbits_should_accept_int_uint_uint_int_as_parameters(const char* pName) {
	int x,y;
	unsigned int p,n;
	setbits(x,p,n,y);
	// test invariantly passes, if no compiler error occurs
	TINYTEST_ASSERT(1);
	return 1;
}

int setbits_should_return_x_if_n_is_zero(const char* pName) {
	int x,y;
	unsigned int p,n;
	n = 0;
	TINYTEST_ASSERT(setbits(x,p,n,y) == x);
	return 1;
}

int setbits_should_return_y_if_n_equals_32_and_p_equals_0(const char* pName) {
	int x,y;
	unsigned int p,n;
	n = 32;
	p = 0;
	TINYTEST_ASSERT(setbits(x,p,n,y) == y);
	return 1;
}

int setbits_should_return_the_31_leftmost_bits_of_x_and_the_rightmost_bit_of_y_if_n_equals_1_and_p_equals_30(const char* pName) {
	int x,y;
	unsigned int p,n;
	
	n = 1;
	p = 30;
	
	srand(time(NULL));
	
	x = rand();
	y = rand();
	
	char x_bits[33], y_bits[33], candidate_bits[33];
	x_bits[32] = 0;
	y_bits[32] = 0;
	candidate_bits[32] = 0;
	bits_of_32bit_signed_int_to_string(x, &(x_bits[0]));
	bits_of_32bit_signed_int_to_string(y, &(y_bits[0]));
	
	int candidate = setbits(x,30,1,y);
	bits_of_32bit_signed_int_to_string(candidate, &(candidate_bits[0]));
	int the_31_leftmost_bits_are_there = (((x >> 1) == (candidate >> 1)));
	int the_rightmost_bit_of_y_is_there = (((~((~0) << 1)) & y) == ((~((~0) << 1)) & candidate));
	TINYTEST_ASSERT(the_31_leftmost_bits_are_there && the_rightmost_bit_of_y_is_there);
	return 1;	
}

int setbits_should_return_y_as_an_appropriate_subsequence_of_x(const char* pName) {
	int x,y;
	unsigned int p,n;
	
	srand(time(NULL));
	
	p = (unsigned int) ((1. * rand() / RAND_MAX) * 32);
	n = (unsigned int) ((1. * rand() / RAND_MAX) * (31 - p));
	
	printf("\n");
	printf("Test 5: p = %u, n = %u\n", p, n);
	
	// TODO: involve negative integers also
	x = (int) ((1. * rand() / RAND_MAX) * INT_MAX);
	y = (int) ((1. * rand() / RAND_MAX) * INT_MAX);

	char x_bits[33], y_bits[33], candidate_bits[33];
	x_bits[32] = 0;
	y_bits[32] = 0;
	candidate_bits[32] = 0;
	bits_of_32bit_signed_int_to_string(x, &(x_bits[0]));
	bits_of_32bit_signed_int_to_string(y, &(y_bits[0]));
	printf("x = %s\ny = %s\n", x_bits, y_bits);
	
	int candidate = setbits(x,p,n,y);
	bits_of_32bit_signed_int_to_string(candidate, &(candidate_bits[0]));
	printf("c = %s\n", candidate_bits);
	
	for (unsigned int i = p; i < p + n; ++i) {
		int candidate_bit = ((candidate >> i) & 1);
		int y_bit = ((y >> i) & 1);
		TINYTEST_ASSERT(candidate_bit == y_bit);
	}
	
	return 1;
}

// add tests to test suite
TINYTEST_START_SUITE(SETBITS);
	TINYTEST_ADD_TEST(setbits_should_accept_int_uint_uint_int_as_parameters,NULL,NULL);
	TINYTEST_ADD_TEST(setbits_should_return_x_if_n_is_zero,NULL,NULL);
	TINYTEST_ADD_TEST(setbits_should_return_y_if_n_equals_32_and_p_equals_0,NULL,NULL);
	TINYTEST_ADD_TEST(setbits_should_return_the_31_leftmost_bits_of_x_and_the_rightmost_bit_of_y_if_n_equals_1_and_p_equals_30,NULL,NULL);
	TINYTEST_ADD_TEST(setbits_should_return_y_as_an_appropriate_subsequence_of_x,NULL,NULL);
TINYTEST_END_SUITE();

// run test suite
TINYTEST_START_MAIN();
	TINYTEST_RUN_SUITE(SETBITS);
TINYTEST_END_MAIN();
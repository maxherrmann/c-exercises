/*
 * Exercise 3-5. Write the function itob (n, s ,b) that converts the integer n
 * into a base b character representation in the string s. In particular,
 * itob (n, s, 16) formats n as a hexadecimal integer in s.
 */
 
/*
 * For an arbitrary base b, we have
 *
 * log_b n = log_10 n / log_10 b.
 *
 * We start with determining the highest exponent of b by computing
 *
 * highest exponent of base = floor(log_10 n / log_10 b)
 *
 * Then we compute the weight b^(heighest exponent of base) with an integer division:
 *
 * n / b^(heighest exponent of base) = weight of heighest exponent of base
 *
 * Then we do the same for every other base^k:
 *
 * (n - weight of heighest exponent of base * b^(heighest exponent of base)) / b^(heighest exponent of base - 1) = weight of 2nd-to-heighest exponent of base
 *
 * The sequence of the weights is then n in the new base.
 *
 * E.g. n = 327, b = 16
 *
 * highest exponent of base = floor(log_10 327 / log_10 16) = 2
 *
 * 327 / 16^2 = 1 = weight of heighest exponent of base
 *
 * (327 - 1 * 16^2) / 16 = 4 = weight of 2nd-to-heighest exponent of base
 *
 * (327 - 1 * 16^2 - 4 * 16) = 7
 *
 * Result is then 147, the sequence of weights for every base to the power of 0 up to 2.
 * 
 */
 
/*
 * Compile with:
 *
 * gcc -o ex3-5_testsuite.exe ex3-5_testsuite.c ..\ex3-5.c
 */
 
#include "..\ex3-5.h"
#include "tinytest.h" 

#include <limits.h>
#include <string.h>
 
int itob_converts_0_into_a_base_2_character_in_string_s() {
	char* number_string;
	itob(0, &number_string, 2);
	if (strcmp(number_string, "0") == 0)
		return 1;
	return 0;
}

int itob_converts_1_into_a_base_2_character_in_string_s() {
	char* number_string;
	itob(1, &number_string, 2);
	if (strcmp(number_string, "1") == 0)
		return 1;
	return 0;	
}

int itob_converts_327_into_a_base_16_character_in_string_s() {
	char* number_string;
	itob(327, &number_string, 16);
	if (strcmp(number_string, "147") == 0)
		return 1;
	return 0;	
}

int itob_converts_minus_1024_into_a_base_15_character_in_string_s() {
	char* number_string;
	itob(-1024, &number_string, 15);
	if (strcmp(number_string, "-484") == 0)
		return 1;
	else
		printf("Expected: -484. Actual: %s\n", number_string);
	return 0;		
}

int itob_can_handle_INT_MAX_with_base_14() {
	char* number_string;
	itob(INT_MAX, &number_string, 14);
	if (strcmp(number_string, "1652CA931") == 0)
		return 1;
	else
		printf("Expected: 1652CA931. Actual: %s\n", number_string);
	return 0;	
}

int itob_can_handle_INT_MIN_with_base_14() {
	char* number_string;
	itob(INT_MIN, &number_string, 14);
	if (strcmp(number_string, "-1652CA932") == 0)
		return 1;
	else
		printf("Expected: -1652CA932. Actual: %s\n", number_string);
	return 0;	
}

TINYTEST_START_SUITE(ITOB);
	TINYTEST_ADD_TEST(itob_converts_0_into_a_base_2_character_in_string_s, NULL, NULL);
	TINYTEST_ADD_TEST(itob_converts_1_into_a_base_2_character_in_string_s, NULL, NULL);
	TINYTEST_ADD_TEST(itob_converts_327_into_a_base_16_character_in_string_s, NULL, NULL);
	TINYTEST_ADD_TEST(itob_converts_minus_1024_into_a_base_15_character_in_string_s, NULL, NULL);
	TINYTEST_ADD_TEST(itob_can_handle_INT_MAX_with_base_14, NULL, NULL);
	TINYTEST_ADD_TEST(itob_can_handle_INT_MIN_with_base_14, NULL, NULL);
TINYTEST_END_SUITE();

TINYTEST_MAIN_SINGLE_SUITE(ITOB);
#include <stdio.h>

#include "tinytest.h"
#include "..\htoi.h"

TINYTEST_DECLARE_SUITE(HTOI);

int htoi_should_accept_strings(const char *pName) {
	htoi("");
	TINYTEST_ASSERT(1);
	return 1;
}

int htoi_should_be_able_to_accept_negative_values(const char *pName) {
	int negativeValue = htoi("-1");
	TINYTEST_ASSERT(negativeValue < 0);
	return 1;
}

int htoi_should_return_an_integer_value(const char *pName) {
	int returnValue;
	// assignment is possible
	TINYTEST_ASSERT(!(returnValue = htoi("")));
	return 1;
}

int htoi_should_convert_0x0_to_0(const char *pName) {
	// 0x0 returns 0
	TINYTEST_ASSERT(!htoi("0x0"));
	return 1;
}

int htoi_should_convert_minus_0x1_to_minus_1(const char* pName) {
	TINYTEST_ASSERT(htoi("-0x1") == -1);
	return 1;
}

int htoi_should_accept_0_to_8_as_input(const char* pName) {
	TINYTEST_ASSERT(htoi("012345678") == 0x12345678);
	return 1;
}

int htoi_should_accept_9_and_A_to_F_as_input(const char* pName) {
	TINYTEST_ASSERT(htoi("9ABCDEF") == 0x9ABCDEF);
	return 1;
}

int htoi_should_accept_a_to_f_as_input(const char* pName) {
	TINYTEST_ASSERT(htoi("abcdef") == 0xABCDEF);
	return 1;
}

TINYTEST_START_SUITE(HTOI);
  TINYTEST_ADD_TEST(htoi_should_accept_strings,NULL,NULL);
  TINYTEST_ADD_TEST(htoi_should_be_able_to_accept_negative_values,NULL,NULL);
  TINYTEST_ADD_TEST(htoi_should_return_an_integer_value,NULL,NULL);
  TINYTEST_ADD_TEST(htoi_should_convert_0x0_to_0,NULL,NULL);
  TINYTEST_ADD_TEST(htoi_should_convert_minus_0x1_to_minus_1,NULL,NULL);
  TINYTEST_ADD_TEST(htoi_should_accept_0_to_8_as_input,NULL,NULL);
  TINYTEST_ADD_TEST(htoi_should_accept_9_and_A_to_F_as_input,NULL,NULL);
  TINYTEST_ADD_TEST(htoi_should_accept_a_to_f_as_input,NULL,NULL);
TINYTEST_END_SUITE();

TINYTEST_START_MAIN();
  TINYTEST_RUN_SUITE(HTOI);
TINYTEST_END_MAIN();
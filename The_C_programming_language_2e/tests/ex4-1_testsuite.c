/*
 *
 * Exercise 4-1. Write the function strrindex (s, t), which returns the position 
 * of the rightmost occurrence of t in s, or -1 if there is none.
 * 
 *
 * Compile with:
 * gcc -o ex4-1_testsuite.exe ex4-1_testsuite.c ..\ex4-1.c
 */
 
#include "tinytest.h"
#include "..\ex4-1.h"

#include <stdio.h>

int strrindex_should_accept_two_pointers_to_characters() {
	char *s, *t;
	strrindex(s, t);
	return 1;
}

int strrindex_should_return_minus_one_if_s_is_empty() {
	char *s = "", *t;
	if (strrindex(s, t) == -1)
		return 1;
	return 0;
}

int strrindex_should_return_minus_one_if_s_is_s_and_t_is_t() {
	char *s = "s", *t = "t";
	if (strrindex(s, t) == -1)
		return 1;
	return 0;
}

int strrindex_should_return_0_if_s_is_s_and_t_is_s() {
	char *s = "s", *t = "s";
	if (strrindex(s, t) == 0)
		return 1;
	return 0;
}

int strrindex_should_return_1_if_s_is_ss_and_t_is_s() {
	char *s = "ss", *t = "s";
	if (strrindex(s, t) == 1)
		return 1;
	return 0;
}

int strrindex_should_return_2_if_s_is_jetplane_and_t_is_tpl() {
	char *s = "jetplane", *t = "tpl";
	if (strrindex(s, t) == 2)
		return 1;
	return 0;
}

int strrindex_should_return_9_if_s_is_uuuussuusssuuu_and_t_is_ss() {
	int position;
	char *s = "uuuussuusssuuu", *t = "ss";
	if ((position = strrindex(s, t)) == 9)
		return 1;
	printf("Expected: 9, Actual: %d\n", position);
	return 0;
}

TINYTEST_START_SUITE(STRRINDEX);
	TINYTEST_ADD_TEST(strrindex_should_accept_two_pointers_to_characters, NULL, NULL);
	TINYTEST_ADD_TEST(strrindex_should_return_minus_one_if_s_is_empty, NULL, NULL);
	TINYTEST_ADD_TEST(strrindex_should_return_minus_one_if_s_is_s_and_t_is_t, NULL, NULL);
	TINYTEST_ADD_TEST(strrindex_should_return_0_if_s_is_s_and_t_is_s, NULL, NULL);
	TINYTEST_ADD_TEST(strrindex_should_return_1_if_s_is_ss_and_t_is_s, NULL, NULL);
	TINYTEST_ADD_TEST(strrindex_should_return_2_if_s_is_jetplane_and_t_is_tpl, NULL, NULL);
	TINYTEST_ADD_TEST(strrindex_should_return_9_if_s_is_uuuussuusssuuu_and_t_is_ss, NULL, NULL);
TINYTEST_END_SUITE();

TINYTEST_MAIN_SINGLE_SUITE(STRRINDEX);
// Exercise 2-9. In a two's complement number system, `x &= (x-1)' deletes the rightmost l-bit in `x'. Explain why. Use this observation to write a faster version of bitcount.

#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <stdio.h>

#include "tinytest.h"
#include "..\bitcount.h"

// declare test suite
TINYTEST_DECLARE_SUITE(BITCOUNT);

// "The C programming language", 2e, Kernighan/Ritchie, p.50
int bitcount_non_optimized(unsigned int x) {
	int b;
	
	for(b = 0; x != 0; x >>= 1)
		if (x == 01) ++b;
	
	return b;
}

int bitcount_should_return_x_if_x_is_zero(const char* pName) {
	TINYTEST_ASSERT(bitcount(0) == 0);
	return 1;
}

int bitcount_should_return_the_same_than_the_non_optimized_version_of_bitcount(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	TINYTEST_ASSERT(bitcount(x) == bitcount_non_optimized(x));
	
	return 1;
}

// add tests to test suite
TINYTEST_START_SUITE(BITCOUNT);
	TINYTEST_ADD_TEST(bitcount_should_return_x_if_x_is_zero,NULL,NULL);
	TINYTEST_ADD_TEST(bitcount_should_return_the_same_than_the_non_optimized_version_of_bitcount,NULL,NULL);
TINYTEST_END_SUITE();

// run test suite
TINYTEST_START_MAIN();
	TINYTEST_RUN_SUITE(BITCOUNT);
TINYTEST_END_MAIN();
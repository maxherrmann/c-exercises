// Exercise 2-7. Write a function `invert(x,p,n)' that returns `x' with the `n' bits that begin at position `p' inverted (i.e., 1 changed into 0 and vice versa), leaving the others unchanged.

#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <stdio.h>

#include "tinytest.h"
#include "..\rightrot.h"
#include "..\setbits.h"

// declare test suite
TINYTEST_DECLARE_SUITE(RIGHTROT);

void bits_of_32bit_signed_int_to_string(int arg, char* string) {
	for(int i = 0; i < 32; ++i) {
		if ((1 << i) & arg)
			string[31 - i] = '1';
		else
			string[31 - i] = '0';
	}
}

int rightrot_should_return_x_if_n_is_zero(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	TINYTEST_ASSERT(rightrot(x,0) == x);
	return 1;
}

int rightrot_should_return_rotated_x_by_one_if_n_is_one(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	int right_rotated_by_one = setbits(x,0,31, x >> 1);
	right_rotated_by_one = setbits(right_rotated_by_one,30,1, x << 31);
	
	char x_bits[33];
	x_bits[32] = 0;
	char right_rotated_by_one_bits[33];
	right_rotated_by_one_bits[32] = 0;
	
	bits_of_32bit_signed_int_to_string(x, x_bits);
	bits_of_32bit_signed_int_to_string(right_rotated_by_one, right_rotated_by_one_bits);
	
	printf("Test 2: x   = %s (%i)\n", &(x_bits[0]), x);
	printf("Test 2: rot = %s\n", &(right_rotated_by_one_bits[0]));
	
	TINYTEST_ASSERT(rightrot(x,1) == right_rotated_by_one);
	return 1;
	
}

int rightrot_should_return_rotated_x_by_two_if_n_is_two(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	int right_rotated_by_two = setbits(x,0,30, x >> 2);
	right_rotated_by_two = setbits(right_rotated_by_two,29,2, x << 30);
	
	char x_bits[33];
	x_bits[32] = 0;
	char right_rotated_by_two_bits[33];
	right_rotated_by_two_bits[32] = 0;
	
	bits_of_32bit_signed_int_to_string(x, x_bits);
	bits_of_32bit_signed_int_to_string(right_rotated_by_two, right_rotated_by_two_bits);
	
	printf("Test 3: x   = %s (%i)\n", &(x_bits[0]), x);
	printf("Test 3: rot = %s\n", &(right_rotated_by_two_bits[0]));
	
	TINYTEST_ASSERT(rightrot(x,2) == right_rotated_by_two);
	return 1;
}

int rightrot_should_be_left_rotated_by_one_if_n_is_31(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	int right_rotated_by_31 = setbits(x,1,30, x << 1);
	right_rotated_by_31 = setbits(right_rotated_by_31,0,1, x >> 31);
	
	char x_bits[33];
	x_bits[32] = 0;
	char right_rotated_by_31_bits[33];
	right_rotated_by_31_bits[32] = 0;
	
	bits_of_32bit_signed_int_to_string(x, x_bits);
	bits_of_32bit_signed_int_to_string(right_rotated_by_31, right_rotated_by_31_bits);
	
	printf("Test 4: x   = %s (%i)\n", &(x_bits[0]), x);
	printf("Test 4: rot = %s\n", &(right_rotated_by_31_bits[0]));
	
	TINYTEST_ASSERT(rightrot(x,31) == right_rotated_by_31);
	return 1;
}

int rightrot_should_return_x_if_n_is_32(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	int right_rotated_by_32 = rightrot(x, 32);
	
	char right_rotated_by_32_bits[33];
	right_rotated_by_32_bits[32] = 0;
	
	bits_of_32bit_signed_int_to_string(right_rotated_by_32, right_rotated_by_32_bits);
	
	printf("Test 5: rot = %s\n", &(right_rotated_by_32_bits[0]));
	
	TINYTEST_ASSERT(rightrot(x,32) == x);
	
	return 1;	
	
}

int rightrot_should_return_x_rotated_by_one_if_n_is_33(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	TINYTEST_ASSERT(rightrot(x,33) == rightrot(x,1));
	
	return 1;	
	
}

int rightrot_should_return_x_rotated_by_two_if_n_is_34(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	TINYTEST_ASSERT(rightrot(x,34) == rightrot(x,2));
	
	return 1;	
	
}

// add tests to test suite
TINYTEST_START_SUITE(RIGHTROT);
	TINYTEST_ADD_TEST(rightrot_should_return_x_if_n_is_zero,NULL,NULL);
	TINYTEST_ADD_TEST(rightrot_should_return_rotated_x_by_one_if_n_is_one,NULL,NULL);
	TINYTEST_ADD_TEST(rightrot_should_return_rotated_x_by_two_if_n_is_two,NULL,NULL);
	TINYTEST_ADD_TEST(rightrot_should_be_left_rotated_by_one_if_n_is_31,NULL,NULL);
	TINYTEST_ADD_TEST(rightrot_should_return_x_if_n_is_32,NULL,NULL);
	TINYTEST_ADD_TEST(rightrot_should_return_x_rotated_by_one_if_n_is_33,NULL,NULL);
	TINYTEST_ADD_TEST(rightrot_should_return_x_rotated_by_two_if_n_is_34,NULL,NULL);
TINYTEST_END_SUITE();

// run test suite
TINYTEST_START_MAIN();
	TINYTEST_RUN_SUITE(RIGHTROT);
TINYTEST_END_MAIN();
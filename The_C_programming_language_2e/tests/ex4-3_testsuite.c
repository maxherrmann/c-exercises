/*
 *
 * Exercise 4-3. Given the basic framework, it's straightforward to extend the calculator.
 * Add the modulus (%) operator and provisions for negative numbers.
 *
 *
 * Compile with:
 * gcc -o ex4-3_testsuite.exe ex4-3_testsuite.c ..\ex4-3.c
 * 
 */
 
#include <stdio.h>
 
#include "tinytest.h"
#include "..\ex4-3.h"

int number_of_calls_to_getop = 0;

int calculator_should_accept_modulo_as_operator_with_two_operands() {
	
}

/* getop() returns either macro NUMBER or a character, and writes it into the given character array */
int first_call_to_getop_mockup_should_return_NUMBER_and_2055() {
	
	double first_operand_floating_point;
	char first_operand_string[64];
	int return_value_of_getop = getop(first_operand_string);
	
	if (return_value_of_getop == NUMBER)
		first_operand_floating_point = atof(first_operand_string);
		if (first_operand_floating_point == 2055.0)
			return 1;
		
	return 0;
}

int second_call_to_getop_mockup_should_return_NUMBER_and_44044() {
	
	double second_operand_floating_point;
	char second_operand_string[64];
	int return_value_of_getop = getop(second_operand_string);
	if (return_value_of_getop == NUMBER)
		second_operand_floating_point = atof(second_operand_string);
		if (second_operand_floating_point == 44044.0)
			return 1;
	return 0;
}

int third_call_to_getop_mockup_should_return_the_character_corresponding_to_modulo() {
	
	char operator_string[64];
	int return_value;
	
	if ((return_value = getop(operator_string)) == '%') {
		return 1;
	}
	printf("Expected: %%. Actual: 0x%x\n", return_value);
	return 0;
	
}

TINYTEST_START_SUITE(EX4_3);
	TINYTEST_ADD_TEST(third_call_to_getop_mockup_should_return_the_character_corresponding_to_modulo, NULL, NULL);
	TINYTEST_ADD_TEST(second_call_to_getop_mockup_should_return_NUMBER_and_44044, NULL, NULL);
	TINYTEST_ADD_TEST(first_call_to_getop_mockup_should_return_NUMBER_and_2055, NULL, NULL);
TINYTEST_END_SUITE();

TINYTEST_MAIN_SINGLE_SUITE(EX4_3);
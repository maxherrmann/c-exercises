// Exercise 2-5. Write the function `any(s1,s2)', which returns the first location in the string `s1' where any character from the string `s2' occurs, or `-1' if `s1' contains no characters from `s2'. (The standard library function `strpbrk' does the same job but returns a pointer to the location.)

#include "tinytest.h"
#include "..\any.h"

TINYTEST_DECLARE_SUITE(ANY);

int any_should_accept_two_strings(const char* pName) {
	char* string1 = "";
	char* string2 = "";
	any(string1, string2);
	// invariantly passed if no compilation errors occur
	TINYTEST_ASSERT(1);
	return 1;
}

int any_should_return_the_first_occurrence_of_pound_sign_at_first_index(const char* pName) {
	char* string1 = "#00";
	char* string2 = "#";
	int index_to_pound_sign = any(string1, string2);
	TINYTEST_ASSERT(!index_to_pound_sign);
	return 1;
}

int any_should_return_the_first_occurrence_of_pound_sign_at_third_index(const char* pName) {
	char* string1 = "00#0";
	char* string2 = "#";
	int index_to_pound_sign = any(string1, string2);
	TINYTEST_ASSERT(index_to_pound_sign == 2);
	return 1;
}

int any_should_return_the_first_occurence_of_pound_sign_at_third_index_even_if_two_pound_signs_are_given(const char* pName) {
	char* string1 = "00#0";
	char* string2 = "##";
	int index_to_pound_sign = any(string1, string2);	
	TINYTEST_ASSERT(index_to_pound_sign == 2);
	return 1;	
}

int any_should_return_the_first_index_of_the_first_occuring_of_two_symbols(const char* pName) {
	char* string1 = "000#&0000";
	char* string2 = "&#";
	int index_to_pound_sign = any(string1, string2);
	TINYTEST_ASSERT(index_to_pound_sign == 3);
	return 1;
}

int any_should_return_minus_1_if_the_queried_symbols_are_not_included(const char* pName) {
	char* string1 = ")";
	char* string2 = "%&=";
	int index_to_one_of_the_queried_symbols = any(string1, string2);
	TINYTEST_ASSERT(index_to_one_of_the_queried_symbols == -1);
	return 1;
}

int any_should_return_minus_1_if_no_symbol_is_queried(const char* pName) {
	char* string1 = "";
	char* string2 = "";
	int index_to_queried_symbol = any(string1, string2);
	TINYTEST_ASSERT(index_to_queried_symbol == -1);
	return 1;
}

TINYTEST_START_SUITE(ANY);
	TINYTEST_ADD_TEST(any_should_accept_two_strings,NULL,NULL);
	TINYTEST_ADD_TEST(any_should_return_the_first_occurrence_of_pound_sign_at_first_index,NULL,NULL);
	TINYTEST_ADD_TEST(any_should_return_the_first_occurrence_of_pound_sign_at_third_index,NULL,NULL);
	TINYTEST_ADD_TEST(any_should_return_the_first_occurence_of_pound_sign_at_third_index_even_if_two_pound_signs_are_given,NULL,NULL);
	TINYTEST_ADD_TEST(any_should_return_the_first_index_of_the_first_occuring_of_two_symbols,NULL,NULL);
	TINYTEST_ADD_TEST(any_should_return_minus_1_if_the_queried_symbols_are_not_included,NULL,NULL);
	TINYTEST_ADD_TEST(any_should_return_minus_1_if_no_symbol_is_queried,NULL,NULL);
TINYTEST_END_SUITE();

TINYTEST_START_MAIN();
	TINYTEST_RUN_SUITE(ANY);
TINYTEST_END_MAIN();
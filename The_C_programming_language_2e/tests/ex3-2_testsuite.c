/*
 * Test suite for exercise 3-2 of "The C Programming Language", 2e:
 *
 * Write a function escape (s, t) that converts characters like
 * newline and tab into visible escape sequences like \n and \t as it copies the
 * string t to s. Use a switch. Write a function for the other direction as well,
 * converting escape sequences into the real characters.
 *
 * Compile with:
 * gcc -o ex3-2_testsuite.exe ex3-2_testsuite.c ..\ex3-2.c
 */
 
#include "..\ex3-2.h"
#include "tinytest.h"
#include <string.h>

int escape_converts_a_single_newline_character_into_its_visible_escape_sequence() {
	char* t = "\n";
	char* s;
	escape(&s, t);
	if (strcmp(s, "\\n") == 0)
		return 1;
	return 0;
}

int escape_converts_character_0x9_into_its_visible_escape_sequence() {
/*
 * "The C Programming Language", 2e, section 2.3 "Constants", p.37:
 *
 * Certain characters can be represented in character and string constants by
 * escape sequences like \n (newline); these sequences look like two characters,
 * but represent only one. In addition, an arbitrary byte-sized bit pattern can be
 * specified by
 *
 * `\ooo'
 *
 * where ooo is one to three octal digits (0...7) or by
 *
 * `\xhh'
 *
 * where hh is one or more hexadecimal digits (0...9, a...f, A...F). So we might
 * write
 * 
 * #define VTAB '\013'   // ASCII vertical tab
 * #define BELL '\017'   // ASCII bell character
 *
 * or, in hexadecimal,
 *
 * #define VTAB '\xb'   // ASCII vertical tab
 * #define BELL '\x7'   // ASCII bell character
 *
 * The complete set of escape sequences is
 *
 *     \a   alert (bell) character
 *     \b   backspace
 *     \f   formfeed
 *     \n   newline
 *     \r   carriage return
 *     \t   horizontal tab
 *     \v   vertical tab
 *     \\   backslash
 *     \?   question mark
 *     \'   single quote
 *     \"   double quote
 *     \ooo octal number
 *     \xhh hexadecimal number
 */
	
	// horizontal tab is 0x9 or 11 (octal)
	char* t = "\11";
	char* s;
	escape(&s, t);
	if (strcmp(s, "\\t") == 0)
		return 1;
	return 0;
}

int escape_knows_all_the_128_ASCII_characters() {
	
	char* t = "\1\2\3\4\5\6\7\10\11\12\13\14\15\16\17\
\20\21\22\23\24\25\26\27\30\31\32\33\34\35\36\37\40\
!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\
[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\177";

	char* expected = "\\001\\002\\003\\004\\005\\006\\a\\b\\t\\n\\v\\f\\r\\016\\017\
\\020\\021\\022\\023\\024\\025\\026\\027\\030\\031\\032\\033\\034\\035\\036\\037 \
!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\
[\\\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\\177";
	
	char* s;
	escape(&s, t);
	if (strcmp(s, expected) == 0)
		return 1;
	return 0;
}

int deescape_can_handle_the_BELL_escape_sequence() {
	char* t = "\\a";
	char* s;
	deescape(&s, t);
	if (strcmp(s, "\a") == 0)
		return 1;
	return 0;
}

int deescape_can_handle_001_022_177_escaped_sequence() {
	char* t = "\\001\\022\\177";
	char* s;
	deescape(&s, t);
	printf("%s\n", s);
	if (strcmp(s, "\1\22\177") == 0)
		return 1;
	return 0;
}

int deescape_knows_all_the_128_ASCII_characters() {
	
	char *t = "\1\2\3\4\5\6\7\10\11\12\13\14\15\16\17\
\20\21\22\23\24\25\26\27\30\31\32\33\34\35\36\37\40\
!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\
[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\177";

	char *s, *t_prime;
	escape(&s, t);
	deescape(&t_prime, s);
	
	if (strcmp(t, t_prime) == 0)
		return 1;
	
	return 0;
}

TINYTEST_START_SUITE(EX3_2);
	TINYTEST_ADD_TEST(escape_converts_a_single_newline_character_into_its_visible_escape_sequence, NULL, NULL);
	TINYTEST_ADD_TEST(escape_converts_character_0x9_into_its_visible_escape_sequence, NULL, NULL);
	TINYTEST_ADD_TEST(escape_knows_all_the_128_ASCII_characters, NULL, NULL);
	TINYTEST_ADD_TEST(deescape_can_handle_the_BELL_escape_sequence, NULL, NULL);
	TINYTEST_ADD_TEST(deescape_can_handle_001_022_177_escaped_sequence, NULL, NULL);
	TINYTEST_ADD_TEST(deescape_knows_all_the_128_ASCII_characters, NULL, NULL);
TINYTEST_END_SUITE();

TINYTEST_MAIN_SINGLE_SUITE(EX3_2);
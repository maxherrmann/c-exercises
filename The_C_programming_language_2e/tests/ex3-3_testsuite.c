/*
 * The C Programming Language, 2e
 *
 * Exercise 3-3. Write a function expand (s1, s2) that expands shorthand notations
 * like a-z in the string s1 into the equivalent complete list abc...xyz in
 * s2. Allow for letters of either case and digits, and be prepared to handle cases
 * like a-b-c and a-z0-9 and -a-z. Arrange that a leading or trailing - is
 * taken literally.
 *
 * Compile with:
 * gcc -o ex3-3_testsuite.exe ex3-3_testsuite.c ..\ex3-3.c
 * 
 */

/* test environment */
#include "..\ex3-3.h"
#include "tinytest.h"
#include <string.h>

/* test 1: expand accepts two strings */
int expand_should_accept_two_strings_as_parameters() {
	char *string_1, *string_2;
	expand(string_1, &string_2);
	return 1;
}

/* test 2: expand should expand the string "a-c" to "abc" */
int expand_should_expand_string_a_to_c_to_abc() {
	char *src = "a-c";
	char *dest;
	expand(src, &dest);
	if (strcmp(dest, "abc") == 0)
		return 1;
	return 0;
}

/* test 3: expand should expand the string "a-z" to "a...z" */
int expand_should_expand_string_a_to_z_to_ab_blabla_yz() {
	char *src = "a-z";
	char *dest;
	expand(src, &dest);
	if (strcmp(dest, "abcdefghijklmnopqrstuvwxyz") == 0)
		return 1;
	return 0;
}

/* test 4: expand should expand the string "b-c" to "bc" */
int expand_should_expand_string_b_to_c_to_bc() {
	char *src = "b-c";
	char *dest;
	expand(src, &dest);
	if (strcmp(dest, "bc") == 0)
		return 1;
	return 0;
}

/* test 5: expand should expand the string "a-b-c" to "abc" */
int expand_should_expand_string_a_to_b_to_c_to_abc() {
	char *src = "a-b-c";
	char *dest;
	expand(src, &dest);
	if (strcmp(dest, "abc") == 0)
		return 1;
	return 0;
}

/* test 6: expand should expand the string "A-C" to "ABC" */
int expand_should_expand_string_A_to_C_to_ABC() {
	char *src = "A-C", *dest;
	expand(src, &dest);
	if (strcmp(dest, "ABC") == 0)
		return 1;
	return 0;
}

/* test 7: expand should expand the string "A-Z" to "A...Z" */
int expand_should_expand_string_A_to_Z_to_AB_blabla_YZ() {
	char *src = "A-Z";
	char *dest;
	expand(src, &dest);
	if (strcmp(dest, "ABCDEFGHIJKLMNOPQRSTUVWXYZ") == 0)
		return 1;
	return 0;
}

/* test 8: expand should expand the string "0-9" to "0...9" */
int expand_should_expand_string_0_to_9_to_01_blabla_89() {
	char *src = "0-9", *dest;
	expand(src, &dest);
	if (strcmp(dest, "0123456789") == 0)
		return 1;
	return 0;
}

/* test 9: expand should expand the string "a-z0-9" to "a...z0...9" */
int expand_should_expand_string_a_to_z0_to_9_to_ab_blabla_yz01_blabla_89() {
	char *src = "a-z0-9", *dest;
	expand(src, &dest);
	if (strcmp(dest, "abcdefghijklmnopqrstuvwxyz0123456789") == 0)
		return 1;
	return 0;
}

/* test 10: expand should expand the string "-a-z" to "-ab...yz" */
int expand_should_expand_string_minus_a_to_z_to_minus_ab_blabla_yz() {
	char *src = "-a-z", *dest;
	expand(src, &dest);
	if (strcmp(dest, "-abcdefghijklmnopqrstuvwxyz") == 0)
		return 1;
	return 0;
}

/* test 11: expand should take a trailing minus (-) literally */
int expand_should_expand_string_a_to_z_minus_to_ab_blabla_yz_minus() {
	char *src = "a-z0-9-", *dest;
	expand(src, &dest);
	if (strcmp(dest, "abcdefghijklmnopqrstuvwxyz0123456789-") == 0)
		return 1;
	return 0;	
}

/* add unit tests to test suite `EX3_3' */
TINYTEST_START_SUITE(EX3_3);
	TINYTEST_ADD_TEST(expand_should_accept_two_strings_as_parameters, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_a_to_c_to_abc, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_a_to_z_to_ab_blabla_yz, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_b_to_c_to_bc, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_a_to_b_to_c_to_abc, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_A_to_C_to_ABC, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_A_to_Z_to_AB_blabla_YZ, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_0_to_9_to_01_blabla_89, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_a_to_z0_to_9_to_ab_blabla_yz01_blabla_89, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_minus_a_to_z_to_minus_ab_blabla_yz, NULL, NULL);
	TINYTEST_ADD_TEST(expand_should_expand_string_a_to_z_minus_to_ab_blabla_yz_minus, NULL, NULL);
TINYTEST_END_SUITE();

/* run tests */
TINYTEST_MAIN_SINGLE_SUITE(EX3_3);
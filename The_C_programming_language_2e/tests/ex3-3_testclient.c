#include "..\ex3-3.h"
#include <stdio.h>
#include <stdlib.h>

#define MAX_NUMBER_OF_CHARACTERS 128

int main() {
	int i = 0;
	char *string = calloc(MAX_NUMBER_OF_CHARACTERS, sizeof(char)), *expanded_string, character;
	while((character = getchar()) != '\n')
		string[i++] = character;
	expand(string, &expanded_string);
	printf("%s\n", expanded_string);
	return 0;
}
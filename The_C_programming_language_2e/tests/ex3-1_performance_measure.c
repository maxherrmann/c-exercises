/*
 * P E R F O R M A N C E   T E S T
 * ===============================
 * binary search one test in loop VS. binary search two tests in loop
 *
 * The test is performed by taking 40 samples of comparing runs of binary search with one test in loop vs. binary search with two tests in loop.
 * The array has 10^5 elements with randomized integer values in the range [0, 10^5 - 1].
 *
 * The value to be searched is always in the range, but chosen at random.
 *
 * As expected, two tests in loop is faster since the one test approach requires a maximum number of splits of the interval (log_2 10^5).
 * The two test approach, instead, uses a varying number of splits, where the mean is (log_2 10^5)/2.
 *
 * Here are some results:
 * 
 * C:\git\C-exercises\The_C_programming_language_2e\tests>ex3-1_performance_measure.exe
 * Mean elapsed time (two tests): 5.895000e-008 sec
 * Mean elapsed time (one test ): 5.912500e-008 sec
 *
 * C:\git\C-exercises\The_C_programming_language_2e\tests>ex3-1_performance_measure.exe
 * Mean elapsed time (two tests): 5.207500e-008 sec
 * Mean elapsed time (one test ): 5.935000e-008 sec
 * 
 * C:\git\C-exercises\The_C_programming_language_2e\tests>ex3-1_performance_measure.exe
 * Mean elapsed time (two tests): 5.655000e-008 sec
 * Mean elapsed time (one test ): 5.932500e-008 sec
 * 
 * C:\git\C-exercises\The_C_programming_language_2e\tests>ex3-1_performance_measure.exe
 * Mean elapsed time (two tests): 5.557500e-008 sec
 * Mean elapsed time (one test ): 5.985000e-008 sec
 * 
 * C:\git\C-exercises\The_C_programming_language_2e\tests>ex3-1_performance_measure.exe
 * Mean elapsed time (two tests): 5.052500e-008 sec
 * Mean elapsed time (one test ): 5.962500e-008 sec
 * 
 * C:\git\C-exercises\The_C_programming_language_2e\tests>ex3-1_performance_measure.exe
 * Mean elapsed time (two tests): 5.662500e-008 sec
 * Mean elapsed time (one test ): 6.015000e-008 sec
 */

#include "..\ex3-1.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void one_test_vs_two_tests(double* time_elapsed_one_test, double* time_elapsed_two_tests) {
				 // get current time
		 time_t current_time;
		 time(&current_time);
		 
		 // use current time as seed for pseudo-random function
		 srand(current_time);
		 
		 // generate number_of_elements random numbers in the range [0, number_of_elements)
		 int number_of_elements = 100000;
		 int* v = calloc(number_of_elements, 4);
		 for (int i = 0; i < number_of_elements; ++i)
			 v[i] = rand() % number_of_elements;
		 int x = v[rand() % number_of_elements];
		 
		 // sort array (ascending order)
		 qsort(
			&(v[0]),                  /* base: initial element of array */
			number_of_elements,       /* nmemb: number of elements in array */
			4,                        /* size in byte of an array element */
			(int (*)(const void*, const void*)) compare_int
		 );
		 
		 // create start and end times for measuring performance
		 clock_t start_time_binary_search, 
				end_time_binary_search, 
				start_time_binary_search_with_exactly_one_test_in_loop, 
				end_time_binary_search_with_exactly_one_test_in_loop;
		
		// set number of repetitions
		int number_of_repetitions = 1000000;
		
		start_time_binary_search = clock();
		for (int i = 0; i < number_of_repetitions; ++i)
			binarysearch(x, v, number_of_elements);
		end_time_binary_search = clock();
				
		start_time_binary_search_with_exactly_one_test_in_loop = clock();
		for (int i = 0; i < number_of_repetitions; ++i) 
			binary_search_with_exactly_one_test_in_loop(x, v, number_of_elements);
		end_time_binary_search_with_exactly_one_test_in_loop = clock();
		 
		 // compute elapsed seconds
		 *time_elapsed_two_tests += ((double) (end_time_binary_search - start_time_binary_search)) / CLOCKS_PER_SEC / number_of_repetitions;
		 *time_elapsed_one_test += ((double) (end_time_binary_search_with_exactly_one_test_in_loop - start_time_binary_search_with_exactly_one_test_in_loop)) / CLOCKS_PER_SEC / number_of_repetitions;
		 
		 // free resources
		 free(v);
}

int main() {
	
	double time_elapsed_two_tests, time_elapsed_one_test;
	int max_number_of_samples = 40;
	
	for (int samples = 0; samples < max_number_of_samples; ++samples) {
		one_test_vs_two_tests(&time_elapsed_one_test, &time_elapsed_two_tests);
	}
	
	printf("Mean elapsed time (two tests): %e sec\n", time_elapsed_two_tests/max_number_of_samples);
	printf("Mean elapsed time (one test ): %e sec\n", time_elapsed_one_test/max_number_of_samples);
		 
	 // default case
	 return 0;
}
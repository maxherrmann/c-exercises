/*
 * The C Programming Language, 2e
 *
 * Exercise 3-1. Our binary search makes two tests inside the loop, when one
 * would suffice (at the price of more tests outside). Write a version with only one
 * test inside the loop and measure the difference in run-time.
 *
 * The function returns the position (a number between 0 and n-1) if x occurs in v, 
 * and -1 if not.
 * 
 */
 
/*
 * Compile with:
 * gcc -o ex3-1_testSuite.exe ..\ex3-1.c ex3-1_testSuite.c 
 */
 
#include "..\ex3-1.h"
#include "tinytest.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
 
// test 1: binary search returns -1 if x does not occur in v
int binary_search_should_return_minus_1_if_x_does_not_occur_in_v () {
	int x = 0;
	int v[] = {1, 1, 1};
	if (binarysearch(x, v, 3) == -1)
		return 1;
	return 0;
}

// test 2: binary search returns 1 if x = 1 and v = (0, 1, 2)
int binary_search_should_return_1_if_x_equals_1_and_v_is_0_1_2 () {
	int x = 1;
	int v[] = {0, 1, 2};
	if (binarysearch(x, v, 3) == 1)
		return 1;
	return 0;
}

// test 3: binary search returns 0 if x = 0 and v = (0, 1, 2)
int binary_search_should_return_0_if_x_equals_0_and_v_is_0_1_2 () {
	int x = 0;
	int v[] = {0, 1, 2};
	if (binarysearch(x, v, 3) == 0)
		return 1;
	return 0;
}

// test 4: binary search returns 2 if x = 2 and v = (0, 1, 2)
int binary_search_should_return_2_if_x_equals_2_and_v_is_0_1_2 () {
	int x = 2;
	int v[] = {0, 1, 2};
	if (binarysearch(x, v, 3) == 2)
		return 1;
	return 0;
}

// test 5: binary search returns the same output than bsearch from the standard library for x = 2 and v = (0, 1, 2)
int binary_search_delivers_the_same_result_than_bsearch_for_x_equals_2_and_v_is_0_1_2 () {
	int x = 2;
	int v[] = {0, 1, 2};
	// call standard library `bsearch'
	int* match = bsearch(
		&x,      /* key: object that needs to be matched */
		&(v[0]), /* base: initial element of v */
		3,       /* nmemb: array has 3 elements */
		4,       /* size: 4 bytes for int(eger)*/
		(int (*)(const void*, const void*)) compare_int /* compare function */
	);
	if (binarysearch(x, v, 3) == *match)
		return 1;
	return 0;
}

// test 6: binary search returns the same result than bsearch from the standard library for randomized value in the range [0, 9], and a randomized three-element array in the same range
int binary_search_delivers_the_same_result_than_randomized_3_element_array () {
	/*
	 * time.h:
	 * 
	 * #ifndef _TIME_T_DEFINED
     * #define _TIME_T_DEFINED
     * #ifdef _USE_32BIT_TIME_T
     *   typedef __time32_t time_t;
     * #else
     *   typedef __time64_t time_t;
     * #endif
     * #endif
	 *
	 * No idea if this means that it is 32-bit or 64-bit.
	 * How can one find out?
	 * Maybe `sizeof( time_t )'?
	 * 
	 * Tried it:
	 * `time_t' has 8 byte.
	 * `sizeof(time_t)' tells.
     * Functions with parameter of type `unsigned int' accept `long unsigned int'.
     * Most signifant bits are truncated.	 
	 */
	 
	 // get current time
	 time_t current_time;
	 time(&current_time);
	 
	 // use current time as seed for pseudo-random function
	 srand(current_time);
	 
	 // generate four random numbers in the range [0, 9]
	 int v[] = {rand() % 10, rand() % 10, rand() % 10};
	 int x = rand() % 10;
	 
	 // sort array (ascending order)
	 qsort(
		&(v[0]), /* base: initial element of array */
		3,       /* nmemb: number of elements in array */
		4,       /* size in byte of an array element */
		(int (*)(const void*, const void*)) compare_int
	 );
	 
	 // test if `binarysearch' and `bsearch' return the same result
	 int* match = bsearch(
		&x,      /* key: object to be matched */
		&(v[0]), /* base: initial element of the array */
		3,       /* nmemb: array contains nmemb objects */
		4,       /* size: size in byte of array element */
		(int (*) (const void*, const void*)) compare_int
	 );
	 int matching_index = binarysearch(x, v, 3);
	  
	 if (match == NULL)
		 if (matching_index == -1) /* both functions return non-match */
			return 1;
		else 
			return 0;
	 else if (*match == *(&(v[0]) + matching_index)) {
		 return 1;
	 }
		 
	 // default case
	 return 0;
}

// test 7: binary search returns the same result than bsearch from the standard library for randomized value in the range [0, 10^3), and a randomized 10^3-element array in the same range
int binarysearch_should_return_the_same_than_bsearch_for_randomized_values_in_a_1000_element_array() {
	 // get current time
	 time_t current_time;
	 time(&current_time);
	 
	 // use current time as seed for pseudo-random function
	 srand(current_time);
	 
	 // generate four random numbers in the range [0, 9]
	 int number_of_elements = 1000;
	 int v[number_of_elements];
	 for (int i = 0; i < number_of_elements; ++i)
		 v[i] = rand() % number_of_elements;
	 int x = rand() % number_of_elements;
	 
	 // sort array (ascending order)
	 qsort(
		&(v[0]),                  /* base: initial element of array */
		number_of_elements,       /* nmemb: number of elements in array */
		4,                        /* size in byte of an array element */
		(int (*)(const void*, const void*)) compare_int
	 );
	 
	 // test if `binarysearch' and `bsearch' return the same result
	 int* match = bsearch(
		&x,                       /* key: object to be matched */
		&(v[0]),                  /* base: initial element of the array */
		number_of_elements,       /* nmemb: array contains nmemb objects */
		4,                        /* size: size in byte of array element */
		(int (*) (const void*, const void*)) compare_int
	 );
	 int matching_index = binarysearch(x, v, number_of_elements);
	  
	 if (match == NULL)
		 if (matching_index == -1) /* both functions return non-match */
			return 1;
		else 
			return 0;
	 else if (*match == *(&(v[0]) + matching_index)) {
		 return 1;
	 }
		 
	 // default case
	 return 0;
}

// test 8: binary search with exactly one test in loop returns -1 if x does not occur in v
int binary_search_with_exactly_one_test_in_loop_should_return_minus_1_if_x_does_not_occur_in_v () {
	int x = 0;
	int v[] = {1, 1, 1};
	if (binary_search_with_exactly_one_test_in_loop(x, v, 3) == -1)
		return 1;
	return 0;
}

// test 9: binary search with exactly one test loop returns the same than bsearch with randomized values and 1000 element array
int binary_search_with_exactly_one_test_in_loop_should_return_the_same_than_bsearch_for_randomized_values_in_a_1000_element_array() {
	 // get current time
	 time_t current_time;
	 time(&current_time);
	 
	 // use current time as seed for pseudo-random function
	 srand(current_time);
	 
	 // generate four random numbers in the range [0, 9]
	 int number_of_elements = 1000;
	 int v[number_of_elements];
	 for (int i = 0; i < number_of_elements; ++i)
		 v[i] = rand() % number_of_elements;
	 int x = rand() % number_of_elements;
	 
	 // sort array (ascending order)
	 qsort(
		&(v[0]),                  /* base: initial element of array */
		number_of_elements,       /* nmemb: number of elements in array */
		4,                        /* size in byte of an array element */
		(int (*)(const void*, const void*)) compare_int
	 );
	 
	 // test if `binarysearch' and `bsearch' return the same result
	 int* match = bsearch(
		&x,                       /* key: object to be matched */
		&(v[0]),                  /* base: initial element of the array */
		number_of_elements,       /* nmemb: array contains nmemb objects */
		4,                        /* size: size in byte of array element */
		(int (*) (const void*, const void*)) compare_int
	 );
	 int matching_index = binary_search_with_exactly_one_test_in_loop(x, v, number_of_elements);
	  
	 if (match == NULL)
		 if (matching_index == -1) /* both functions return non-match */
			return 1;
		else 
			return 0;
	 else if (*match == *(&(v[0]) + matching_index)) {
		 return 1;
	 }
		 
	 // default case
	 return 0;
}
 
TINYTEST_START_SUITE(EX3_1);
	TINYTEST_ADD_TEST(binary_search_should_return_minus_1_if_x_does_not_occur_in_v, NULL, NULL);
	TINYTEST_ADD_TEST(binary_search_should_return_1_if_x_equals_1_and_v_is_0_1_2, NULL, NULL);
	TINYTEST_ADD_TEST(binary_search_should_return_0_if_x_equals_0_and_v_is_0_1_2, NULL, NULL);
	TINYTEST_ADD_TEST(binary_search_should_return_2_if_x_equals_2_and_v_is_0_1_2, NULL, NULL);
	TINYTEST_ADD_TEST(binary_search_delivers_the_same_result_than_bsearch_for_x_equals_2_and_v_is_0_1_2, NULL, NULL);
	TINYTEST_ADD_TEST(binary_search_delivers_the_same_result_than_randomized_3_element_array, NULL, NULL);
	TINYTEST_ADD_TEST(binarysearch_should_return_the_same_than_bsearch_for_randomized_values_in_a_1000_element_array, NULL, NULL);
	TINYTEST_ADD_TEST(binary_search_with_exactly_one_test_in_loop_should_return_minus_1_if_x_does_not_occur_in_v, NULL, NULL);
	TINYTEST_ADD_TEST(binary_search_with_exactly_one_test_in_loop_should_return_the_same_than_bsearch_for_randomized_values_in_a_1000_element_array, NULL, NULL);
TINYTEST_END_SUITE();

TINYTEST_MAIN_SINGLE_SUITE(EX3_1);
/*
* Exercise 3-6. Write a version of itoa that accepts three arguments instead of
* two. The third argument is a minimum field width; the converted number must
* be padded with blanks on the left if necessary to make it wide enough.
*/

/*
 * Compile with:
 *
 * gcc -o ex3-6_testsuite.exe ex3-6_testsuite.c ..\ex3-6.c ..\ex3-4.c
 */

#include "..\ex3-6.h"
#include "tinytest.h"

#include <stdio.h>
#include <string.h>

int integer_to_ascii_should_do_nothing_if_field_width_is_zero_or_less() {
	char* number_string = integer_to_ascii_with_field_width(0, 0);
	if (strcmp(number_string, "0") == 0)
		return 1;
	else
		printf("Expected: 0. Actual: %s", number_string);
	
	return 0;
}

int integer_to_ascii_should_return_356_if_field_width_is_set_to_2() {
	char* number_string = integer_to_ascii_with_field_width(356, 2);
	if (strcmp(number_string, "356") == 0)
		return 1;
	else
		printf("Expected: 0. Actual: %s", number_string);
	
	return 0;	
}

int integer_to_ascii_should_return___1066_if_field_width_is_set_to_6() {
	char* number_string = integer_to_ascii_with_field_width(1066, 6);
	if (strcmp(number_string, "  1066") == 0)
		return 1;
	else
		printf("Expected: 0. Actual: %s", number_string);
	
	return 0;	
}

TINYTEST_START_SUITE(ITOA);
	TINYTEST_ADD_TEST(integer_to_ascii_should_do_nothing_if_field_width_is_zero_or_less, NULL, NULL);
	TINYTEST_ADD_TEST(integer_to_ascii_should_return_356_if_field_width_is_set_to_2, NULL, NULL);
	TINYTEST_ADD_TEST(integer_to_ascii_should_return___1066_if_field_width_is_set_to_6, NULL, NULL);
TINYTEST_END_SUITE();

TINYTEST_MAIN_SINGLE_SUITE(ITOA);
/*
 * The C Programming Language, 2e
 *
 * In a two's complement number representation, our version of
 * itoa does not handle the largest negative number, that is, the value of n equal
 * to -(2^{wordsize-l}). Explain why not. Modify it to print that value correctly,
 * regardless of the machine on which it runs.
 *
 */
 
/*
 *
 * From the two's complement, it follows that
 *
 * -INT_MIN = INT_MIN.
 *
 * Along with the C99 characteristic that sign of divisor is the same than modulo,
 *
 * INT_MIN % 10
 *
 * yields a negative number, e.g. -8, s.t.
 *
 * -8 + 0x30 != '8'
 *
 * where 0x30 corresponds to '0'.
 *
 *
 *
 * Modification:
 * 
 * Instead of negating INT_MIN, we negate INT_MIN % 10, and (INT_MIN % 10) % 10 ...
 * 
 */
 
/*
 * Compile with:
 *
 * gcc -o ex3-4_testsuite.exe ex3-4_testsuite.c ..\ex3-4.c
 */

#include "..\ex3-4.h"
#include "tinytest.h"

#include <stdio.h>
#include <string.h>
#include <limits.h>
 
int integer_to_ascii_should_return_string_corresponding_to_8() {
	if (strcmp(integer_to_ascii(8), "8") == 0)
		return 1;
	return 0;
}

int integer_to_ascii_should_return_string_corresponding_to_16() {
	char* ascii_string = integer_to_ascii(16);
	if (strcmp(ascii_string, "16") == 0)
		return 1;
	else
		printf("Actual: %s. Expected: %s\n", ascii_string, "16");
	return 0;	
}

int integer_to_ascii_can_handle_0() {
	if (strcmp(integer_to_ascii(0), "0") == 0)
		return 1;
	return 0;	
}

int integer_to_ascii_should_return_string_corresponding_to_minus_128() {
	char* ascii_string = integer_to_ascii(-128);
	if (strcmp(ascii_string, "-128") == 0)
		return 1;
	else
		printf("Actual: %s. Expected: %s\n", ascii_string, "-128");
	return 0;	
}

int integer_to_ascii_can_handle_INT_MAX() {
	if (strcmp(integer_to_ascii(INT_MAX), "2147483647") == 0)
		return 1;
	return 0;	
}

int integer_to_ascii_can_handle_INT_MIN() {
	if (strcmp(integer_to_ascii(INT_MIN), "-2147483648") == 0)
		return 1;
	return 0;	
}

TINYTEST_START_SUITE(INTEGER_TO_ASCII);
	TINYTEST_ADD_TEST(integer_to_ascii_should_return_string_corresponding_to_8, NULL, NULL);
	TINYTEST_ADD_TEST(integer_to_ascii_should_return_string_corresponding_to_16, NULL, NULL);
	TINYTEST_ADD_TEST(integer_to_ascii_can_handle_0, NULL, NULL);
	TINYTEST_ADD_TEST(integer_to_ascii_should_return_string_corresponding_to_minus_128, NULL, NULL);
	TINYTEST_ADD_TEST(integer_to_ascii_can_handle_INT_MAX, NULL, NULL);
	TINYTEST_ADD_TEST(integer_to_ascii_can_handle_INT_MIN, NULL, NULL);
TINYTEST_END_SUITE();

TINYTEST_MAIN_SINGLE_SUITE(INTEGER_TO_ASCII);
// Exercise 2-7. Write a function `invert(x,p,n)' that returns `x' with the `n' bits that begin at position `p' inverted (i.e., 1 changed into 0 and vice versa), leaving the others unchanged.

#include <stdlib.h>
#include <time.h>
#include <limits.h>

#include "tinytest.h"
#include "..\invert.h"
#include "..\setbits.h"

// declare test suite
TINYTEST_DECLARE_SUITE(INVERT);

int invert_should_return_x_if_n_is_zero(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	unsigned int p = (unsigned int) (((1. * rand()) / RAND_MAX) * 31);
	
	TINYTEST_ASSERT(invert(x,p,0) == x);
	return 1;
}

int invert_should_return_the_first_bit_of_x_inverted_if_n_is_one_and_p_is_zero(const char* pName) {
	
	srand(time(NULL));
	int x = (int) (((1. * rand()) / RAND_MAX) * INT_MAX);
	
	TINYTEST_ASSERT(invert(x,0,1) == setbits(x,0,1,~x));
	
}

// add tests to test suite
TINYTEST_START_SUITE(INVERT);
	TINYTEST_ADD_TEST(invert_should_return_x_if_n_is_zero,NULL,NULL);
	TINYTEST_ADD_TEST(invert_should_return_the_first_bit_of_x_inverted_if_n_is_one_and_p_is_zero,NULL,NULL);
TINYTEST_END_SUITE();

// run test suite
TINYTEST_START_MAIN();
	TINYTEST_RUN_SUITE(INVERT);
TINYTEST_END_MAIN();
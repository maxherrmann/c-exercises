/* copies the string `t' to `s', printing non-printing characters */
void escape(char** s, const char* t);
/* encodes escaped characters */
void deescape(char** s, const char* t);
// Write the function `htoi(s)', which converts a string of hexadecimal digits (including an optional `0x' or `0X') into its equivalent integer value. The allowable digits are `0' to `9', `a' through `f', and `A' to `F'.

// 0000123   valid
// 0x00322   valid
// -0x9A97   valid
// ++78401   invalid

#include <stdlib.h>

int htoi(const char* s) {
	char* tailingString;
	return strtol(s, &tailingString, 16);
}
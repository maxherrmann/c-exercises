// Exercise 2-5. Write the function `any(s1,s2)', which returns the first location in the string `s1' where any character from the string `s2' occurs, or `-1' if `s1' contains no characters from `s2'. (The standard library function `strpbrk' does the same job but returns a pointer to the location.)

#include <string.h>

int any(const char* s1, const char* s2) {
	char* pointer_to_first_occurrence;
	pointer_to_first_occurrence = strpbrk(s1, s2);
	if (!pointer_to_first_occurrence)
		return -1;
	return (int) (pointer_to_first_occurrence - s1);
}
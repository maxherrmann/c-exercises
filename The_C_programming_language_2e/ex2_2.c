// Write a loop equivalent to the `for' loop above without using && and ||.
//
// for (i = 0; i < lim-1 && (c = getchar()) != '\n' && c != EOF; ++i)
// 		s[i] = c;

#include <stdio.h>

int main() {
	printf("Type characters. End by <CR>.\n");
	int lim = 7;
	char s[7];
	int c;
	s[6] = '\0';
	for (int i = 0; i < lim-1; ++i)
		if ((c = getchar()) != '\n')
			if (c != EOF)
				s[i] = c;
	printf("You typed: %s.\n", s);
	return 0;
}
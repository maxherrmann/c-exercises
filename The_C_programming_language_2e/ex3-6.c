#include "ex3-4.h"

#include <stdlib.h>

char* integer_to_ascii_with_field_width(int arg, int field_width) {
	int number_of_digits = 0, pad_width;
	char* number_string = integer_to_ascii(arg);
	
	// count digits
	int i = 0, j = 0;
	while(number_string[i++])
		++number_of_digits;
    pad_width = field_width - number_of_digits;
	
	// left-pad with blanks
	if (pad_width > 0) {
		char* left_padded_string = calloc(field_width + 1, sizeof(char));
		for (; j < pad_width; ++j)
			left_padded_string[j] = ' ';
		for (j = 0; j < number_of_digits; ++j)
			left_padded_string[j + pad_width] = number_string[j];
		// terminating null character
		left_padded_string[j + pad_width] = '\0';
		return left_padded_string;
	}
	return number_string;
}
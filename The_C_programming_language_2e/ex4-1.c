#include <stdlib.h>

char* reverse(char* arg) {
	int number_of_characters_in_arg = 0, i, j;
	char* reversed_string;
	
	// count number of characters in arg
	while(arg[number_of_characters_in_arg++])
		;
	--number_of_characters_in_arg;
	
	// allocate space
	reversed_string = calloc(number_of_characters_in_arg + 1, sizeof(char));
	
	// reverse order
	reversed_string[number_of_characters_in_arg] = '\0';
	for (i = number_of_characters_in_arg - 1, j = 0; i > -1; --i, ++j)
		reversed_string[i] = arg[j];
	
	return reversed_string;
	
}

/* 
 * return first occurence of t in s, -1 if none 
 *
 * The following code is taken from Kernighan/Ritchie "The C Programming Language", 2e, page 69
 */
int strindex(char s[], char t[]) {
	int i, j, k;
	
	for (i = 0; s[i] != '\0'; i++) {
		for (j=i, k=0; t[k]!='\0' && s[j]==t[k]; j++, k++)
			;
		if (k > 0 && t[k] == '\0')
			return i;
	}
	return -1;
}

/* return the rightmost position of t in s */
int strrindex(char* s, char* t) {
	
	int number_of_characters_in_s = 0, number_of_characters_in_t = 0, position;
	char *rs, *rt;
	
	/* count number of characters in s and t */
	while(s[number_of_characters_in_s++])
		;
	--number_of_characters_in_s;
	while(t[number_of_characters_in_t++])
		;
	--number_of_characters_in_t;
	
	/* reverse order of strings */
	rs = reverse(s);
	rt = reverse(t);
	
	if ((position = strindex(rs, rt)) == -1)
		return -1;
	return number_of_characters_in_s - position - number_of_characters_in_t;
}
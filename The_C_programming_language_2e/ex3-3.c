#include <stdlib.h>

void do_expand(char first, char last, char *string) {
	
	/* compute number of letters */
	int i, number_of_letters = last - first + 1;
	
	/* write every character in range [first, last] */
	for (i = 0; i < number_of_letters; ++i)
		string[i] = first + i;
}

void expand(const char *src, char **dest) {
	char current;
	int i = 0, j = 0, number_of_symbols = 0;
	
	/* parse */
	while ((current = src[i])) {
		
		/* minus (-) triggers expansion */
		if ((i>0) && (src[i+1] != '\0') && (current == '-')) {
			
			number_of_symbols += (src[i+1] - src[i-1]);
			i += 2;
		}			
		else {
			++number_of_symbols;
			++i;
		}		
		
	}
	
	/* allocate space */
	*dest = calloc(number_of_symbols, sizeof(char));
	
	/* expand */
	i = 0;
	while ((current = src[i])) {
		
		/* minus (-) triggers expansion */
		if ((i>0) && (src[i+1] != '\0') && (current == '-')) {
			
			/* write every character in range [first, last] */
			do_expand(src[i-1], src[i+1], *dest + j - 1);
			j += (src[i+1] - src[i-1]);
			i += 2;
		}
		else {
			(*dest)[j] = src[i];
			++i, ++j;
		}
		
	}
	
	/* terminating null character */
	(*dest)[j] = '\0';

}
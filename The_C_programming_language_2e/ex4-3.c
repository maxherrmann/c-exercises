#include "ex4-3.h"

#include <stdio.h>

int getop(char s[]) {
	
	if (number_of_calls_to_getop == 0) {
		
		s[0] = '2';
		s[1] = '0';
		s[2] = '5';
		s[3] = '5';
		s[4] = '\0';
		
		++number_of_calls_to_getop;
		
		return NUMBER;
	}
	
	if (number_of_calls_to_getop == 1) {
		
		s[0] = '4';
		s[1] = '4';
		s[2] = '0';
		s[3] = '4';
		s[4] = '4';
		s[5] = '\0';
		
		++number_of_calls_to_getop;
		
		return NUMBER;
		
	}
	
	if (number_of_calls_to_getop == 2) {
		
		s[0] = '%';
		s[1] = '\0';
		return '%';
		
	}
	
	return NUMBER;
}
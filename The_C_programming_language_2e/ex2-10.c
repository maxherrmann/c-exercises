// Exercise 2-10. Rewrite the function `lower', which converts upper case letters to lower case, with a conditional expression instead of if-else.

int lower(int arg) {
	
	return ((arg > 0x40) && (arg < 0x5B))  ? (arg + 0x20) : -1;
	
}
// Exercise 2-7. Write a function `invert(x,p,n)' that returns `x' with the `n' bits that begin at position `p' inverted (i.e., 1 changed into 0 and vice versa), leaving the others unchanged.

#include "setbits.h"

int invert(int x, unsigned int p, unsigned int n) {
	
	return setbits(x,p,n,~x);
	
}
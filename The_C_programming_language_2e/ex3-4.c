#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* reverse_string(char* arg) {
	// determine length of string (characters that precede the terminating null character)
	size_t number_of_characters = strlen(arg);
	char* reversed_string = calloc(number_of_characters + 1, sizeof(char));
	
	// reverse order
	int j = 0;
	for (int i = number_of_characters - 1; i > -1; --i, ++j)
		reversed_string[j] = arg[i];
	
	// terminating null character
	reversed_string[j] = '\0';
	
	return reversed_string;
}

char* integer_to_ascii(int arg) {
	
	int current_digit, remaining_integer, i;
	char* string = calloc(32, sizeof(char));
	
	// initialize loop over integer digits
	remaining_integer = arg;
	i = 0;
	do {
		
		// least significant digit
		current_digit = remaining_integer % 10;
		if (current_digit < 0) 
			// negative value
			string[i] = -current_digit + 0x30;
		else
			// non-negative value
			string[i] = current_digit + 0x30;
		
		// remove least significant digit
		remaining_integer /= 10;	
			
		++i;
		
	} while (remaining_integer != 0);
	
	// if number is negative, append minus sign
	if (arg < 0) {
		string[i++] = '-';
	}
		
	// terminate string with 0 character
	string[i] = '\0';
	
	return reverse_string(string);
}
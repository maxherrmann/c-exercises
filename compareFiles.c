#include <stdio.h>

int compareFiles(char* file1, char* file2, int maxNumberOfCharacters) {
	
	/* Declare local variables */
	int bufferFull, characterCount, character1st, character2nd, equal;
	
	/* Initial states */
	bufferFull = 0;
	characterCount = 0;
	equal = 0;
	
	/* Open files for reading */
	FILE* fileHandle1st = fopen(file1, "r");
	FILE* fileHandle2nd = fopen(file2, "r");
	
	/* Read file contents character-wise until condition holds */
	while(!bufferFull) {
		
		/* Read single character from file 1 */
		character1st = fgetc(fileHandle1st);
		
		/* Read single character from file 2 */
		character2nd = fgetc(fileHandle2nd);
		
		/* End comparison if characters mismatch */
		if (character1st != character2nd) {
			equal = -1;
			break;
		}
		
		/* Stop reading if end of file is reached or if character represents a line break */
		if (character1st == EOF || character1st == '\n') break;
		if (character2nd == EOF || character2nd == '\n') break;
		
		/* 
		 * Increment character count.
		 * If buffer size is not yet exceeded, continue.
		 */
		bufferFull = !(++characterCount < maxNumberOfCharacters);
	}
	
	/* Close files */
	fclose(fileHandle1st);
	fclose(fileHandle2nd);
	
	return equal;
}